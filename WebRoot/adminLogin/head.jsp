<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="../WEB-INF/lib/c.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>头部</title>
	<link rel="stylesheet" type="text/css" href="css/public.css" />
	
  </head>
  <body>
   <div class="head">
		<div class="headL">
		</div>
		<div class="headM">
			<h1 style="font-size:30px;">CURD后台管理系统</h1>
		</div>
		<div class="headR">
			<span style="color:#FFF">
			欢迎：
			<% 
				if(session.getAttribute("userName") == null)
				{
    				response.sendRedirect("../login.action");
				}
				else
				{
					out.print(session.getAttribute("userName"));
				}
			%>
			</span> 
			<a href="../login.action" rel="external"target="_top">【退出】</a>
		</div>
	</div>
	
</body>
</html> 