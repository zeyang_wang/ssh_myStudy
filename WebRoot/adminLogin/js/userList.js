  layui.use('layer','table', function(){
  var layer = layui.layer
  ,table = layui.table //表格
  
}); 

/**
 * 用户列表新增
 */
  function addUser(){
	  layer.open
	  ({
		  type: 2,
		  title: "新增用户：",
          area: ['400px','240px'],
		  shadeClose: true,
		  closeBtn: 2,
		  skin: 'yourclass',
		  content: ['adminLogin/userAdd.jsp','no'],
		  shade: 0.5,//不显示遮罩层
		}); 
  }
  /**
   *删除用户信息
   */
  function deleteUser(id) {
	  layer.open({
		  type:0
		  ,closeBtn:2
		  ,btn: ['确认删除','取消']
	  ,btnAlign: 'c' //按钮居中
		  ,shade: 0 //不显示遮罩
		  ,yes: function(){
			  window.location.href="Login_deleteUser.action?id="+id;
		  }
	  ,no: function(){
		  layer.close();
	  }
	  })
  }
  function closeLay(){
	  var index = parent.layer.getFrameIndex(window.name);
	  parent.layer.close(index);
  }
  