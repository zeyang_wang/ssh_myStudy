<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
 <head>
    <title>左侧</title>
<link rel="stylesheet" type="text/css" href="../adminLogin/css/public.css" />
 </head>
  
  <body>
	<!-- 左边节点 -->
	<div class="container">
		<div class="leftsidebar_box">
			<a href="javascript:location.reload();" target="main"><div class="line">
					<img src="../adminLogin/img/coin01.png" />&nbsp;&nbsp;首&nbsp;&nbsp;页
				</div></a>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin03.png" />
					<img class="icon2" src="../adminLogin/img/coin04.png" /> 日志管理
					<img class="icon3" src="../adminLogin/img/coin19.png" />
					<img class="icon4" src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks" href="dayCard_userList.action"
						target="main">日志管理</a><img class="icon5" src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin07.png" /><img class="icon2"
						src="../adminLogin/img/coin08.png" /> 会员管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a href="Login_userList.action" target="main"
						class="cks">会员管理</a><img class="icon5" src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin10.png" /><img class="icon2"
						src="../adminLogin/img/coin09.png" /> 其他管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">其他管理</a><img class="icon5"
						src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin11.png" /><img class="icon2"
						src="../adminLogin/img/coin12.png" /> 新闻管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">新闻管理</a><img class="icon5" src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin14.png" /><img class="icon2"
						src="../adminLogin/img/coin13.png" /> 心愿管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">心愿管理</a><img class="icon5" src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin15.png" /><img class="icon2"
						src="../adminLogin/img/coin16.png" /> 预警管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">预警管理</a><img class="icon5"
						src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coin17.png" /><img class="icon2"
						src="../adminLogin/img/coin18.png" /> 收支管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">收支管理</a><img class="icon5"
						src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>
			<dl class="system_log">
				<dt>
					<img class="icon1" src="../adminLogin/img/coinL1.png" /><img class="icon2"
						src="../adminLogin/img/coinL2.png" /> 系统管理<img class="icon3"
						src="../adminLogin/img/coin19.png" /><img class="icon4"
						src="../adminLogin/img/coin20.png" />
				</dt>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a href="changepwd.html"
						target="main" class="cks">修改密码</a><img class="icon5"
						src="../adminLogin/img/coin21.png" />
				</dd>
				<dd>
					<img class="coin11" src="../adminLogin/img/coin111.png" /><img class="coin22"
						src="../adminLogin/img/coin222.png" /><a class="cks">退出</a><img
						class="icon5" src="../adminLogin/img/coin21.png" />
				</dd>
			</dl>

		</div>

	</div>   
   
	<script type="text/javascript" src="../adminLogin/js/jquery.min.js"></script>
	<script type="text/javascript" src="../adminLogin/js/public.js"></script> 
  </body>
</html>
