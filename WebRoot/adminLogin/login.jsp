<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<link rel="stylesheet" type="text/css" href="<%=basePath%>/adminLogin/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/adminLogin/css/demo.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/adminLogin/css/component.css" />
 <head>
    <base href="<%=basePath%>">
    
    <title>后台登录中心</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="container demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="logo_box">
						<h3>欢迎登录</h3>
						<form method="post" action="Login_toLogin">
							<div class="input_outer">
								<span class="u_user"></span>
								<input name="userName" id="userName" onblur="checkUserName()" class="text" style="color: #FFFFFF !important;background-color:transparent;" type="text" placeholder="请输入用户名">
							</div>
							<div class="input_outer">
								<span class="us_uer"></span>
								<input name="password" id="password" onblur="checkPassWord()" class="text" style="color: #FFFFFF !important;background-color:transparent; position:absolute; z-index:100;"value="" type="password" placeholder="请输入密码">
							</div>
							<input class="act-but submit"value="登录" style="color: #FFFFFF;width:100%;border: none;" type="submit">
						</form>
					</div>
				</div>
			</div>
		</div>
		<script src="<%=basePath%>/adminLogin/js/jquery-1.11.2.min.js"></script>
		<script src="<%=basePath%>/adminLogin/js/TweenLite.min.js"></script>
		<script src="<%=basePath%>/adminLogin/js/EasePack.min.js"></script>
		<script src="<%=basePath%>/adminLogin/js/layer/layer.js"></script>
		<script src="<%=basePath%>/adminLogin/js/demo-1.js"></script>
		<script src="<%=basePath%>/adminLogin/js/rAF.js"></script>
		<div style="text-align:center;">
	</div>
  </body>
</html>
