<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'user.jsp' starting page</title>
    

	<link rel="stylesheet" type="text/css" href="<%=basePath%>adminLogin/css/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=basePath%>adminLogin/css/base.css" />
	<link rel="stylesheet" href="<%=basePath%>js/layui/css/layui.css">
<script type="text/javascript">
	function updataUser(id){
		layer.msg("修改！");
		layer.open(
			//Login_findById.action?id=<s:property value='id'/>
			offset: ['100px', '50px'],
			type: 2, 
  			content: "'Login_findById.action?id='+id+','no'" //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
		);
	}

</script>
  </head>
  
  <body>
	<!-- user页面样式 -->
	<div class="connoisseur">
		<div class="conform">
			<form method="post" action="Login_seach" target="main">
				<div class="cfD">
					<input class="userinput" type="text"name="userName" placeholder="输入用户名" />&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;
					<input class="userinput vpr" type="date"name="time" placeholder="选择日期" />
					<button class="userbtn" type="submit">查询</button>
				</div>
			</form>
		</div>
		<div style=" margin: 10 auto auto 3;">
      	<!-- layui 2.2.5 新增 -->
     		<button class="layui-btn  layui-btn-normal"onclick="addUser()"style="width: 160px;">新增</button>
    	</div>
		<!-- user 表格 显示 -->
		<div class="conShow">
			<table border="1" cellspacing="0" cellpadding="0">
				<tr>
					<td width="46px" class="tdColor tdC">序号</td>
					<td width="400px" class="tdColor">用户名</td>
					<td width="400px" class="tdColor">密码</td>
					<td width="500px" class="tdColor">添加时间</td>
					<td width="400px" class="tdColor">操作</td>
				</tr>
				<s:iterator value="userList" status="s"  var="u">  
				<tr height="50px">
					<td><s:property value="#s.index+1"/></td>
					<td><s:property value="userName"/></td>
					<td><s:property value="password"/></td>
					<td><s:property value="time"/>  <br/></td>
					<td>
					<a onclick="updataUser('<s:property value="id"/>')">
						<img class="operation" src="<%=basePath%>adminLogin/img/update.png">
					</a>
					<img class="operation" onclick="deleteUser(<s:property value='id'/>)" src="<%=basePath%>adminLogin/img/delete.png"></td>
				 </tr>
				</s:iterator>
			</table>
		</div>
			<!-- user 表格 显示 end-->
		<!-- user页面样式end -->
		<div class="paging">
			<!-- PageNum -->
			<ul id="PageNum">
			<li><a href="#">首页</a></li>
			<li><a href="#">上一页</a></li>
			<li><a href="#">第1页</a></li>
			<li><a href="#">下一页</a></li>
			<li><a href="#">尾页</a></li>
			<li><a href="#">共12页</a></li>
			</ul>
			<!-- /PageNum -->
		</div>
	</div>
	<script src="<%=basePath%>js/layui/layui.js" type="text/javascript"></script>
	<script src="<%=basePath%>js/layUI.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>adminLogin/js/userList.js"></script>
  </body>
</html>
