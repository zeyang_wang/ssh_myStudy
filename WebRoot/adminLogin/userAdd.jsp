<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>用户新增窗口</title>
    
	
<link rel="stylesheet" type="text/css" href="<%=basePath%>adminLogin/css/css.css" />

  </head>
  
  <body>
  <!-- 会员注册页面样式 -->
	<div class="baBody">
	<form action="Login_addUser.action" method="post" target="main">
		<div class="bbD">
			&nbsp;&nbsp;&nbsp;用户名：<input type="text"name="userName" class="input3" />
		</div>
		<div class="bbD">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;密码：<input type="password"name="password"
				class="input3" />
		</div>
		<div class="bbD">
			<p class="bbDP">
				<button class="btn_ok btn_yes" type="submit">确定</button>
				<a class="btn_ok btn_no" onclick="closeLay()">取消</a>
			</p>
		</div>
	</form>
	</div>
	<!-- 会员注册页面样式end -->
	<script type="text/javascript" src="<%=basePath%>adminLogin/js/userList.js"></script>
	<script type="text/javascript" src="<%=basePath%>adminLogin/js/layer/layer.js"></script>
	<script type="text/javascript" src="<%=basePath%>adminLogin/js/jquery.min.js"></script>
  </body>
</html>
