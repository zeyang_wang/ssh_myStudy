<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>用户编辑窗口</title>
    
	
<link rel="stylesheet" type="text/css" href="<%=basePath%>adminLogin/css/css.css" />

  </head>
  
  <body>
  <!-- 会员注册页面样式 -->
	<div class="baBody">
	<s:form action="Login_updateUser.action" method="post" target="main">
		<s:hidden name="u.id" value="%{u.id}"/>
		<s:hidden name="u.type" value="%{u.type}"/>
		<s:textfield name="u.userName" label="用户名" value="%{u.userName}" />
		<s:textfield name="u.password" label="密码" value="%{u.password}"/>
		<s:submit value="确定更新"/>
	</s:form>
	</div>
	<!-- 会员注册页面样式end -->
	<script type="text/javascript" src="<%=basePath%>adminLogin/js/jquery.min.js"></script>
  </body>
</html>
