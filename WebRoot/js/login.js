

/**
 * 验证用户名不为空
 */
function checkUserName(){
	debugger;
	var userName = $.trim($("#username").val());
	var falg = true;
	if(userName == null || userName == "" || userName == "用户名"){
		layer.tips("请输入用户名", "#userName", {tips: [2, "#E34127"],time: 500});
		falg = false;
	}
	return falg;
}
/**
 * 自动变更背景图
 */
var imgs =["1.jpg", "2.jpg", "3.jpg"];    //（设定想要显示的图片）
var x = 0;       
        function time1(){
               x++;   
               x=x%3;         //         超过2则取余数，保证循环在0、1、2之间
               document.getElementById("div_first").src ="./image/"+imgs[x];
        }setInterval("time1()",3000);
/**
 * 验证密码不为空
 */
function checkPassWord(){
	var password = $.trim($("#password").val());
	var falg = true;
	if(password == null || password == "" || password == "密码"){
		layer.tips("请输入密码", "#password", {tips: [2, "#E34127"],time: 500});
		falg = false;
	}
	return falg;
}

/**
 * 用户登录
 */
function login(){
	if(checkUserName()&&checkPassWord()){
		var userName = $.trim($("#username").val());
		var passWord = $.trim($("#password").val());
		$.ajax({
			type : "post",
			url : project + "/login/login",
			async : false,
			data : {
				userName : userName,
				passWord : passWord
			},
			dataType:"json",
			success : function(data){
				if(data == "1"){
					layer.msg("登录成功", {icon: 6}, function(){
						location.href = project + "/wel/welcome";
					});
				}else{
					layer.msg("用户名或密码错误", {icon: 5});
				}
			}
		});
	}
}

//回车触发登录
function keyLogin(e){
	  var theEvent = window.event || e;
	      var code = theEvent.keyCode || theEvent.which;
	   if (code==13) {  //回车键的键值为13
		   login();  //调用提交登录
	   }
	  }
		