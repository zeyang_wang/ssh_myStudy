package com.joe.action;


import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import com.joe.hibernate.TUser;
import com.joe.hibernate.TUserDAO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class LoginAction extends ActionSupport implements ModelDriven<TUser>{
	/*
	 *用户登录
	 */
	private TUser u = new TUser();
	private TUserDAO udao = new TUserDAO();
	private Map<String,Object> session = ActionContext.getContext().getSession();
	private static List<TUser> userList;
	public String toLogin(){
		if(udao.findByExample(u).size() > 0){
			//用户名存入session
			session.put("userName", u.getUserName());
			return SUCCESS;
		}
		return ERROR;
	}
	/*
	 * 注销登录
	 */
	public String logout() {
		if(session.get("userName") != null)
		{
			session.remove("userName");//注销登录
		}
		return "logout";
	}
	/*
	 * 获取用户列表
	 */
	@SuppressWarnings("unchecked")
	public String userList(){
		userList = udao.findAll();
		return "userList";
	}
	/*
	 * 条件查询查询用户列表
	 */
	@SuppressWarnings("unchecked")
	public String seach() {
		userList = udao.findFuzzyQuery(u);
		return "seachUser";
	}
	/*
	 * 编号查询查询用户列表
	 */
	public String findById(){
		Integer id = u.getId();
		u = udao.findById(id);
		System.out.println(u);
		return "findById";
	}
	/*
	 * 新增用户
	 */
	public String addUser(){
		u.setTime(new Date());
		u.setType("2");
		udao.save(u);
		return "addUser";
	}
	/*
	 * 编辑用户
	 */
	public String updateUser(){
		u.setTime(new Date());
		udao.attachDirty(u);
		return "updateUser";
	}
	/*
	 * 删除用户
	 */
	public String deleteUser(){
		String id =  ServletActionContext.getRequest().getParameter("id");
		System.out.println(id);
		u = udao.findById(Integer.parseInt(id));
		udao.delete(u);
		return "deleteUser";
	}
	/*
	 *  获取模型驱动
	 */
	public TUser getModel() {
		return this.u;
	}
	public TUser getU() {
		return u;
	}
	public void setU(TUser u) {
		this.u = u;
	}
	/*
	 * 查询的list集合get、set之后可以在jsp页面调用。
	 */
	public List<TUser> getUserList() {
	    return userList;
	}
	@SuppressWarnings("static-access")
	public void setUserList(List<TUser> userList) {
	    this.userList = userList;
	}
 }
