package com.joe.action;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;

import com.joe.hibernate.dayCard;
import com.joe.hibernate.dayCardDAO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class dayCardAction extends ActionSupport implements ModelDriven<dayCard>{
	private static final int UTF = 0;
	/*
	 *用户登录
	 */
	private dayCard u = new dayCard();
	private dayCardDAO udao = new dayCardDAO();
	private Map<String,Object> session = ActionContext.getContext().getSession();
	private static List<dayCard> userList;
	private static Map<Object, Object> dayCardMap = new HashMap<Object,Object>();
	
	public List<dayCard> getUserList() {
		return userList;
	}
	public void setUserList(List<dayCard> userList) {
		dayCardAction.userList = userList;
	}
	public static Map<Object, Object> getDayCardMap() {
		return dayCardMap;
	}
	public static void setDayCardMap(Map<Object, Object> dayCardMap) {
		dayCardAction.dayCardMap = dayCardMap;
	}
	/*
	 * 注销登录
	 */
	public String logout() {
		if(session.get("userName") != null)
		{
			session.remove("userName");//注销登录
		}
		return "logout";
	}
	/*
	 * 获取用户列表
	 */
	
	@SuppressWarnings("unchecked")
	public String userList(){
		userList = udao.findAll();
		return "dayCardsList";
	}
	
	/*
	 * 获取用户列表
	 */
	@SuppressWarnings("unchecked")
	//在Action层，将数据封装成JSON对象。并通过ServletResponse对象输出
	public void dayCardsList(){
		userList = udao.findAll();
		// map中的数据将会被Struts2转换成JSON字符串，所以这里要先清空其中的数据
		if(!dayCardMap.isEmpty()){
			dayCardMap.clear();
		}
		boolean bool = false;
		JSONArray jsonArray = null;
		JSONArray result = null;
        if(!userList.isEmpty()){
    		jsonArray = JSONArray.fromObject(userList);
        }
        dayCardMap.put("userList", jsonArray);
        dayCardMap.put("success", bool);
        System.out.println("map:"+dayCardMap);
        result = JSONArray.fromObject(dayCardMap);
        //SSH框架中struts2的Action中获取response对象的方法
        //实例化一个变量response用来向页面传值
        HttpServletResponse response =ServletActionContext.getResponse();
        //response.setContentType("text/xml; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");	
        response.setContentType("application/json"); 
		try {
			//JDK中PrintWriter类实例化
			PrintWriter out = response.getWriter();
			
			//PrintWriter类的print方法将转好格式的json数据打印
			out.print(result);
			//PrintWriter类的flush()这个函数是清空的意思，用于清空缓冲区的数据流
			out.flush();
			//PrintWriter类的close()关闭读写流
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//需要return null；因为不希望struts导航到其他的地方
        //return;
	}
	/*
	 * 条件查询查询用户列表
	 */
	@SuppressWarnings("unchecked")
	public String seach() {
		userList = udao.findFuzzyQuery(u);
		return "seachUser";
	}
	/*
	 *  获取模型驱动
	 */
	public dayCard getModel() {
		return this.u;
	}
	public dayCard getU() {
		return u;
	}
	public void setU(dayCard u) {
		this.u = u;
	}
	

 }
