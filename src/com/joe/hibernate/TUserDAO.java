package com.joe.hibernate;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.joe.hibernatesessionfactory.HibernateSessionFactory;

public class TUserDAO {
	private static final Logger log = LoggerFactory.getLogger(TUserDAO.class);

	public void save(TUser transientInstance) {
		log.debug("saving TUser instance");
		Session session = null;
        Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
            tx = session.beginTransaction();
            session.save(transientInstance);
			log.debug("save successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void delete(TUser persistentInstance) {
		log.debug("deleting TUser instance");
		Session session = null;
        Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
            tx = session.beginTransaction();
            session.delete(persistentInstance);
			log.debug("delete successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public TUser findById(Integer id) {
		log.debug("getting TUser instance with id: " + id);
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			TUser instance = (TUser) session.get(
					"com.joe.hibernate.TUser", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findByExample(TUser instance) {
		log.debug("finding TUser instance by example");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			List results = session
					.createCriteria("com.joe.hibernate.TUser")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding TUser instance with property: " + propertyName
				+ ", value: " + value);
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from TUser as model where model."
					+ propertyName + "= ?";
			Query queryObject = session.createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findAll() {
		log.debug("finding all TUser instances");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from TUser";
			Query queryObject = session.createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
	public List findFuzzyQuery(TUser u){
		log.debug("finding FuzzyQuery TUser instances");
		SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd");
		String time = ""; 
		if(u.getTime()!=null){
			time = times.format(u.getTime());
		}
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from TUser where userName like '%"+
					u.getUserName() + "%'";
			Query queryObject = session.createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
	public TUser merge(TUser detachedInstance) {
		log.debug("merging TUser instance");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			TUser result = (TUser) session.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void attachDirty(TUser instance) {
		log.debug("attaching dirty TUser instance");
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.update(instance);
			log.debug("attach successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void attachClean(TUser instance) {
		log.debug("attaching clean TUser instance");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			session.buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
}