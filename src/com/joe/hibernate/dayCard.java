package com.joe.hibernate;

import java.util.Date;

/**
 * TUser entity. @author MyEclipse Persistence Tools
 */

public class dayCard implements java.io.Serializable {

	// Fields
	private String uuid;
	private String title;
	private String content;
	private String creatTime;
	private String editTime;
	private String state;
	private String deleteTime;
	private String dealPerson;


	// Constructors

	/** default constructor */
	public dayCard() {
	}
	
	/** full constructor */
	public dayCard(String title, String content, String creatTime, String state,String dealPerson) {
		this.title = title;
		this.content = content;
		this.creatTime = creatTime;
		this.state = state;
		this.dealPerson = dealPerson;
	}
	


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getCreatTime() {
		return creatTime;
	}


	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}


	public String getEditTime() {
		return editTime;
	}


	public void setEditTime(String editTime) {
		this.editTime = editTime;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getDeleteTime() {
		return deleteTime;
	}


	public void setDeleteTime(String deleteTime) {
		this.deleteTime = deleteTime;
	}


	public String getDealPerson() {
		return dealPerson;
	}


	public void setDealPerson(String dealPerson) {
		this.dealPerson = dealPerson;
	}

	

}