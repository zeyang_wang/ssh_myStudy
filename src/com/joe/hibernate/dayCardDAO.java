package com.joe.hibernate;

import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.joe.hibernatesessionfactory.HibernateSessionFactory;

public class dayCardDAO {
	private static final Logger log = LoggerFactory.getLogger(dayCardDAO.class);

	public void save(dayCard transientInstance) {
		log.debug("saving dayCard instance");
		Session session = null;
        Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
            tx = session.beginTransaction();
            session.save(transientInstance);
			log.debug("save successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void delete(dayCard persistentInstance) {
		log.debug("deleting dayCard instance");
		Session session = null;
        Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
            tx = session.beginTransaction();
            session.delete(persistentInstance);
			log.debug("delete successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public dayCard findById(String id) {
		log.debug("getting dayCard instance with id: " + id);
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			dayCard instance = (dayCard) session.get(
					"com.joe.hibernate.dayCard", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findByExample(dayCard instance) {
		log.debug("finding dayCard instance by example");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			List results = session
					.createCriteria("com.joe.hibernate.dayCard")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding dayCard instance with property: " + propertyName
				+ ", value: " + value);
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from dayCard as model where model."
					+ propertyName + "= ?";
			Query queryObject = session.createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public List findAll() {
		log.debug("finding all dayCard instances");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from dayCard";
			Query queryObject = session.createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
	public List findFuzzyQuery(dayCard u){
		log.debug("finding FuzzyQuery dayCard instances");
		SimpleDateFormat times = new SimpleDateFormat("yyyy-MM-dd");
		String time = ""; 
		if(u.getCreatTime()!=null){
			time = times.format(u.getCreatTime());
		}
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			String queryString = "from dayCard where userName like '%"+
					u.getDealPerson() + "%'";
			Query queryObject = session.createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
	public dayCard merge(dayCard detachedInstance) {
		log.debug("merging dayCard instance");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			dayCard result = (dayCard) session.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void attachDirty(dayCard instance) {
		log.debug("attaching dirty dayCard instance");
		Session session = null;
		Transaction tx = null;
		try {
			session = HibernateSessionFactory.getSession();
			tx = session.beginTransaction();
			session.update(instance);
			log.debug("attach successful");
			tx.commit();
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}

	public void attachClean(dayCard instance) {
		log.debug("attaching clean dayCard instance");
		Session session = null;
		try {
			session = HibernateSessionFactory.getSession();
			session.buildLockRequest(LockOptions.NONE).lock(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		} finally {
            HibernateSessionFactory.closeSession();
        }
	}
}