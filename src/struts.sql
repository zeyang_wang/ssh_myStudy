/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50641
Source Host           : 127.0.0.1:3306
Source Database       : struts

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2018-09-30 16:45:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for daycard
-- ----------------------------
DROP TABLE IF EXISTS `daycard`;
CREATE TABLE `daycard` (
  `uuid` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COMMENT '内容',
  `creatTime` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建时间',
  `editTime` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '修改时间',
  `state` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '是否删除状态，1（默认）存在，0删除',
  `deleteTime` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '删除时间',
  `dealPerson` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of daycard
-- ----------------------------
INSERT INTO `daycard` VALUES ('1', '2', '3', '1', null, null, '', '1');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `menuName` varchar(32) DEFAULT NULL COMMENT '菜单名称',
  `menuType` varchar(16) DEFAULT NULL COMMENT '菜单级别',
  `typeID` int(32) DEFAULT NULL COMMENT '所属菜单id',
  `menuDescribe` varchar(32) DEFAULT NULL COMMENT '菜单描述',
  `operationDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '操作日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_menu
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最近登录时间',
  `type` varchar(255) DEFAULT NULL COMMENT '用户类型',
  `isOnline` varchar(50) DEFAULT NULL COMMENT '是否在线',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'zhangsan', '1', '2018-09-01 20:28:22', '1', null);
INSERT INTO `t_user` VALUES ('2', 'wzy', '1', '2018-09-01 20:31:25', '2', null);
INSERT INTO `t_user` VALUES ('3', '1222', '1', '2018-09-30 16:19:37', '1', '1');
